﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

/// <summary>
/// LoginManagerがAttachされているInspector上で匿名で接続ができるスクリプト
/// </summary>
namespace DreamWorld
{
    [CustomEditor(typeof(LoginManager))]
    public class LoginManagerEditorScript : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            EditorGUILayout.HelpBox("Photonサーバーへの接続を担当します", MessageType.Info);

            LoginManager loginManager = (LoginManager)target;

            if (GUILayout.Button("匿名で接続"))
            {
                loginManager.ConnectAnonymously();
            }
        }
    }
}
