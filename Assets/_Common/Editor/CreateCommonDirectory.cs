#if UNITY_EDITOR
using System.IO;
using UnityEditor;

public class CreateCommonDirectory :EditorWindow
{
    [MenuItem("Custom/CreateCommonDirectory")]
    static void CreateEditorDirectory()
    {
        SetDirectory();
        CreateBuildDirectory();
    }

    static void SetDirectory()
    {
        if (AssetDatabase.IsValidFolder("Assets/_Common"))
        {
            return;
        }
        //cf) https://qiita.com/OKsaiyowa/items/f7b2d331526e2a6938b1

        //フォルダを作成・追加
        CreateDirectory("_Scenes");                                              //ゲームのシーンを置く
        CreateDirectory("Prefabs");                                             //ゲーム内で繰り返し使用するObject（Prefab）を置く
        CreateDirectory("Scripts");                                              //ゲーム内で使うスクリプト（.csや.js）を置く
        CreateDirectory("Effects/Animations/AnimatorController");             //ゲーム内で使うアニメーションを置く
        CreateDirectory("Effects/Animations/AnimatorClip");
        CreateDirectory("Effects/Models");
        CreateDirectory("Effects/Materials");                                           //ゲーム内のObjectに設定するマテリアルデータ（色とか光沢とか）を置く
        CreateDirectory("Physics Materials");                               //ゲーム内の物理エンジンで使用するPhysics Materialsを置く
        CreateDirectory("Effects/Shaders");
        CreateDirectory("Effects/Light");
        CreateDirectory("Fonts");                                               //ゲーム内のフォントデータを置く
        CreateDirectory("Effects/Textures");                                            //画像ファイルを置く
        CreateDirectory("Audios");                                                 //ゲーム内のBGMやSEなどのサウンドデータを置く
        CreateDirectory("Resources");                                         //（特殊フォルダ）Resources.Loadで読み込むファイルを置く
        CreateDirectory("Plugins");                                            //（特殊フォルダ）ネイティブプラグインやその他プラグインを置く


    }


    static void CreateDirectory(string folderName)
    {
        string path = "Assets/_Common/" + folderName;

        if (!Directory.Exists(path))
        {
            Directory.CreateDirectory(path);        //指定したディレクトリを作成
        }
        AssetDatabase.ImportAsset(path);        //パスからアセットをインポート
    }

    //Build時にもフォルダを自動生成
    static void CreateBuildDirectory()
    {
        string path = "Build/";

        if (!Directory.Exists(path))
        {
            Directory.CreateDirectory(path);
        }
        AssetDatabase.ImportAsset(path);
    }
}
#endif