﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

/// <summary>
/// RoomManagerを用いてInspector上でRoomの作成や参加をできるようにするスクリプト
/// </summary>
namespace DreamWorld
{
    [CustomEditor(typeof(RoomManager))]
    public class RoomManagerEdiotrScript : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            EditorGUILayout.HelpBox("Roomの作成・参加を担当します", MessageType.Info);

            RoomManager roomManager = (RoomManager)target;
            if (GUILayout.Button(SceneIndexes.SCHOOL.ToString() + "に参加"))
            {
                roomManager.OnEnterButtonClicked_School();
            }

            if(GUILayout.Button(SceneIndexes.OUTDOOR.ToString() + "に参加"))
            {
                roomManager.OnEnterButtonClicked_Outdoor();
            }
        }
    }
}
