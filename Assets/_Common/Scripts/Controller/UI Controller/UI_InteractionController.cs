﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;
using UnityEngine.InputSystem;
using UnityEngine.Events;
using System;

/// <summary>
/// UIとコントローラーのアクティブ・非アクティブ化を管理するスクリプト
/// </summary>
public class UI_InteractionController : MonoBehaviour
{
    [SerializeField]
    GameObject UIController;

    [SerializeField]
    GameObject BaseController;

    [SerializeField]
    InputActionReference inputActionReference_UISwitcher;

    bool isUICanvasActive = false;

    [SerializeField]
    GameObject UICanvasGameobject;

    private void OnEnable()
    {
        inputActionReference_UISwitcher.action.performed += ActivateUIMode;
    }
    private void OnDisable()
    {
        inputActionReference_UISwitcher.action.performed -= ActivateUIMode;

    }

    private void Start()
    {
        // デフォルトで UI Canvas ゲームオブジェクトを非アクティブ化
        if (UICanvasGameobject !=null)
        {
            UICanvasGameobject.SetActive(false);
        }

       //デフォルトでUIコントローラーを非アクティブ化
        UIController.GetComponent<XRRayInteractor>().enabled = false;
        UIController.GetComponent<XRInteractorLineVisual>().enabled = false;
    }

    /// <summary>
    /// このメソッドは、プレイヤーがデフォルトの入力アクションで定義された入力アクションである UI スイッチャー ボタンを押したときに呼び出される。
    /// 呼び出し後、UI Canvas の以前の状態に応じて、UI インタラクション モードのオンとオフが切り替えを行う。
    /// </summary>
    /// <param name="obj"></param>
    private void ActivateUIMode(InputAction.CallbackContext obj)
    {
        if (!isUICanvasActive)
        {
            isUICanvasActive = true;
            // XR Ray Interactor と XR Interactor Line Visual を有効にして UI コントローラーをアクティブ化する
            UIController.GetComponent<XRRayInteractor>().enabled = true;
            UIController.GetComponent<XRInteractorLineVisual>().enabled = true;

            //XR Direct Interactor を無効にして Base Controller を無効にする
            BaseController.GetComponent<XRDirectInteractor>().enabled = false;

            //UI キャンバス ゲームオブジェクトをアクティブにする
            UICanvasGameobject.SetActive(true);

        }
        else
        {
            isUICanvasActive = false;

            UIController.GetComponent<XRRayInteractor>().enabled = false;
            UIController.GetComponent<XRInteractorLineVisual>().enabled = false;
            BaseController.GetComponent<XRDirectInteractor>().enabled = true;

            UICanvasGameobject.SetActive(false);
        }
    }
}
