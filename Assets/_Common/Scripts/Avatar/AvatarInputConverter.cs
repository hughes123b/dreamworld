﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Inspector上で設定したavatarの身体位置情報を取得後、
/// 頭、身体、手の動きを同期させるスクリプト
/// </summary>
public class AvatarInputConverter : MonoBehaviour
{
    /// Inspector
    [Header("Avatar Transform")]
    [SerializeField] private Transform m_MainAvatarTransform;
    [SerializeField] private Transform m_AvatarHead;
    [SerializeField] private Transform m_AvatarBody;

    [SerializeField] private Transform m_AvatarHand_Left;
    [SerializeField] private Transform m_AvatarHand_Right;

    [Header("XRRig Transforms")]
    [SerializeField] private Transform m_XRHead;

    [SerializeField] private Transform m_XRHand_Left;
    [SerializeField] private Transform m_XRHand_Right;

    [SerializeField] private Vector3 m_HeadPositionOffset;
    [SerializeField] private Vector3 m_HandRotationOffset;

    [Header("変化比率")]
    [SerializeField, Range(0.0f, 1.0f)] private float m_Ratio = 0.5f;
    [SerializeField, Range(0.0f, 1.0f)] private float m_BodyRotationRatio = 0.05f;

    
    void Update()
    {
        //Head and Body sync
        m_MainAvatarTransform.position = Vector3.Lerp(m_MainAvatarTransform.position, m_XRHead.position + m_HeadPositionOffset, m_Ratio);
        m_AvatarHead.rotation = Quaternion.Lerp(m_AvatarHead.rotation, m_XRHead.rotation, m_Ratio);
        m_AvatarBody.rotation = Quaternion.Lerp(m_AvatarBody.rotation, Quaternion.Euler(new Vector3(0, m_AvatarHead.rotation.eulerAngles.y, 0)), m_BodyRotationRatio);

        //Hands sync
        m_AvatarHand_Right.position = Vector3.Lerp(m_AvatarHand_Right.position,m_XRHand_Right.position, m_Ratio);
        m_AvatarHand_Right.rotation = Quaternion.Lerp(m_AvatarHand_Right.rotation,m_XRHand_Right.rotation, m_Ratio) * Quaternion.Euler(m_HandRotationOffset);

        m_AvatarHand_Left.position = Vector3.Lerp(m_AvatarHand_Left.position,m_XRHand_Left.position, m_Ratio);
        m_AvatarHand_Left.rotation = Quaternion.Lerp(m_AvatarHand_Left.rotation,m_XRHand_Left.rotation, m_Ratio) * Quaternion.Euler(m_HandRotationOffset);
    }
}
