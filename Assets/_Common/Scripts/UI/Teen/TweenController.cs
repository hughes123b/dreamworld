﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 本スクリプトを空Managerオブジェクト等にAttachすることにより、
/// シリアライズしたオブジェクトにDOTweenを実装する
/// </summary>
namespace DreamWorld
{
    public class TweenController : DOTweenManager
    {
        [SerializeField]
        private Button m_StartButton = null;
        [SerializeField]
        private Image m_BgImage = null;
        [SerializeField]
        private CanvasGroup m_CanvasGroup = null;

        #region Unity Methods
        private void Awake()
        {
            if (m_StartButton == null) return;
            if (m_BgImage == null) return;
            ChangeLocalScale();
            m_BgImage.gameObject.SetActive(false);
        }

        private void Start()
        {
            m_CanvasGroup = m_BgImage.GetComponent<CanvasGroup>();
            ChangeLocalScale();

            ///////////////////
            //if (m_BgImage.gameObject.activeInHierarchy == true) return;
            m_BgImage.gameObject.SetActive (true);
            OnFadeIN();
        }

        #endregion

        #region protected Methods

        /// <summary>
        /// StartButtonをBase.Ratioの比率でnew Vector3(1,1,0)からScaleを
        /// base.Durationの遅延で繰り返し拡大縮小する
        /// </summary>
        protected void ChangeLocalScale()
        {
            var loop = -1;
            var basePos = new Vector3(1, 1, 0);
            transform.DOScale(basePos * base.Ratio, base.Duration)
                .SetLoops(loop, LoopType.Yoyo);
        }

        #endregion
        #region public Methods


        #endregion

        //public void test()
        //{
        //    m_CanvasGroup.alpha = 0;
        //    m_CanvasGroup.interactable = false;
        //    m_BgImage.gameObject.SetActive(true);
        //    m_CanvasGroup.DOFade(1.0F, base.Duration);
        //}

        protected void OnFadeIN()
        {
            CanvasGroputFadeInOut.FadeIn(m_CanvasGroup, base.Duration);

        }
    }
}