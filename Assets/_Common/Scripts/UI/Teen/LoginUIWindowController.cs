﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// LeanTween管理スクリプト
/// Closeボタンを押下時、LoginUIパネルをアニメーション付きで閉じる
/// </summary>
namespace DreamWorld
{
    public sealed class LoginUIWindowController : MonoBehaviour
    {
        // Inspector
        [SerializeField, Tooltip("LeanTweenのAnimationの開始から終了までの時間")]
        private float m_TweenTime = 0.5f;

        // Member
        public float TweenTime { get => m_TweenTime; }

    }
}