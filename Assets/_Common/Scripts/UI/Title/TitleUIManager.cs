﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.InputSystem;
using DG.Tweening;

/// <summary>
/// TitleSceneのUIを管理するスクリプト
/// </summary>
namespace DreamWorld
{
    [RequireComponent(typeof(TweenController))]
    public sealed class TitleUIManager : MonoBehaviour
    {
        // Static
        public static readonly int s_HashFadeIn = Animator.StringToHash("FadeIn");

        // Inspector
        [SerializeField]
        private Image m_Image;
        [SerializeField]
        private TextMeshProUGUI m_TextMesh_1 = null;
        [SerializeField]
        private TextMeshProUGUI m_TextMesh_2 = null;
        [SerializeField]
        private TextMeshProUGUI m_TextMesh_3 = null;
        [SerializeField]
        private TextMeshProUGUI m_TextMesh_4 = null;

        // Member
        private float m_TransitionTime = 1.5f;         // シーンの遷移時間
        private float m_Duration = 3.5f;         // 待ち時間
        private GameObject m_Obj = null;
        private GameObject[] m_Objs = null;
        private Dictionary<string, int> m_TextDatas = new Dictionary<string, int>();
        private int[] m_Datas = { };

        void Awake()
        {
            if (m_Image == null) return;
            m_Image.gameObject.SetActive(false);
        }

        private void Start()
        {
            // m_Animator = m_Image.GetComponent<Animator>();
            m_Objs = GameObject.FindGameObjectsWithTag("Title");
            for(var i = 0;i < m_Objs.Length; i++)
            {
                
                if(i > 0) { m_Objs[i].SetActive(false); }
            }
                //m_TextMesh_1 = GetComponent<TextMeshProUGUI>();
                var str = m_TextMesh_1.text;
                m_TextMesh_1.text = "";
                m_TextMesh_1.DOText(str, 4.0f).SetEase(Ease.Linear).OnComplete(NextText);
        }

        private void NextText()
        {
            m_Objs[0].SetActive(false);
            m_Objs[1].SetActive(true);
        }

        private void DOTextEaseLinear(string text, int i)
        {
            m_TextMesh_1.DOText(text, 3.0f).SetEase(Ease.Linear).OnComplete(DOTextFadeIn);
        }

        private void DOTextFadeIn()
        {
            // フェード
            //DOTween.Sequence()
            //    .Append(textMeshPro.DOFade(1, duration / 4))
            //    .AppendInterval(duration / 2)
            //    .Append(textMeshPro.DOFade(0, duration / 4));
            //var obj = GetComponent<TextMesh>().gameObject;
        }

        private IEnumerator LoadLevel()
        {
            // 全てのtext_1,2,3,4のテキストでDoTwe.DOTextを用いて、文字をAnimationと共にノベルゲーム風に表示

            foreach (var obj in m_Objs)
            {
                TextMeshProUGUI textMesh = obj.GetComponent<TextMeshProUGUI>();
                var str = textMesh.text;
                textMesh.text = "";

                textMesh.DOText(str, 3.0f).SetEase(Ease.Linear).OnComplete(DOTweenOnComplete);
            }
            // その後、表示したテキストを非表示にし、次のテキストを表示へ


            //それを１から４のテキスト全てに実行させる


            yield return StartCoroutine("BeforeProcess");

            yield return new WaitForSeconds(m_TransitionTime);
            SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex + 1);
            //m_Animator.SetBool(s_HashFadeIn, true);
        }

        public void DOTweenOnComplete()
        {
            //シリアライズしたオブジェクトを参照し、GameObjectを取得後、破棄、非表示
            // go.SetActive(false);
        }
    }
}