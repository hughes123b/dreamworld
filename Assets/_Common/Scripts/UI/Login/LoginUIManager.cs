﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Login時に使用するUIの管理マネージャー
/// </summary>
namespace DreamWorld
{
    public sealed class LoginUIManager : MonoBehaviour
    {
        // Inspector
        [Header("LoginUIプレハブのGameobject等を設定")]
        [SerializeField, Tooltip("LoginUIをWindowを設定")]
        private GameObject m_LoginUI = null;
        [SerializeField, Tooltip("LoginUIを開いたときに最初に表示されるUI")]
        private GameObject m_ConnectOptionPanel = null;
        [SerializeField, Tooltip("「名前で接続」選択時\n表示されるInput FieldがあるUI")]
        private GameObject m_ConnectWithNamePanel = null;

        [SerializeField, Tooltip("押下時UnityEditor、Gameが終了するボタンを設定")]
        private Button m_Exitbutton = null;
        [SerializeField, Tooltip("Exitボタン押下時の終了確認Windowを開く")]
        private GameObject m_ConfirmWindow = null;

        // Member
        private bool isLoginUIActive = false;

        #region Unity Methods
        private void Start()
        {
            ActiveOptionPannel();
            m_ConfirmWindow.SetActive(false);
            m_Exitbutton.onClick.AddListener(() => ExitApplication());
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// OptionsPanel・NamePanelの表示、非表示をチェックし、
        /// UIパネルがnull場合にGameObjectを探し設定
        /// </summary>
        private void ActiveOptionPannel()
        {
            if (m_ConnectOptionPanel == null || m_ConnectWithNamePanel == null)
            {
                var connectOptinal = GameObject.FindGameObjectWithTag("OptionsPanel");
                var connectWithNamePanel = GameObject.FindGameObjectWithTag("NamePanel");
                connectOptinal.SetActive(true);
                connectWithNamePanel.SetActive(false);
            }
            else
            {
                m_ConnectOptionPanel.SetActive(true);
                m_ConnectWithNamePanel.SetActive(false);
            }
        }
        /// <summary>
        /// アプリケーションを終了させる
        /// </summary>
        private void ExitApplication()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
        }

        private void ActiveUIMode()
        {
            if (!isLoginUIActive)
            {
                isLoginUIActive = true;

            }
        }
        #endregion
    }
}
