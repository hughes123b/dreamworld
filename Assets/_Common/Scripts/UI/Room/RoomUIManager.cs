﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

/// <summary>
/// RoomSceneのUI管理スクリプト
/// </summary>
namespace DreamWorld
{
    public sealed class RoomUIManager : MonoBehaviour
    {
        // Inspector
        [SerializeField, Tooltip("RoomUIの右上「ｘ」のUIを閉じるボタン")]
        private Button m_WindowCloseButton = null;
        [SerializeField, Tooltip("RoomUIのwindow自体を設定")]
        private GameObject m_RoomUIWindow = null;

        #region UI Interation Methods
        public void OnClickCloseButton()
        {
            m_RoomUIWindow.transform.DOScale(Vector3.zero, 0.5f);
        }

        #endregion
    }
}