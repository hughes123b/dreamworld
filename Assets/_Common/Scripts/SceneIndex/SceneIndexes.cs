using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SceneIndexes : byte
{
    TITLE = 0,
    LOGIN,
    HOME,
    SCHOOL,
    OUTDOOR
}