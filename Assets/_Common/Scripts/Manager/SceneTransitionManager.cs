﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// シーン遷移管理スクリプト
/// </summary>
namespace DreamWorld
{
    public sealed class SceneTransitionManager : MonoBehaviour
    {
        public void OnTransitionToLoginScene()
        {
            SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex + 1);
        }
        public void OnTransitionToHomeScene()
        {

        }
        public void OnTransitionToTestScene()
        {

        }
    }
}