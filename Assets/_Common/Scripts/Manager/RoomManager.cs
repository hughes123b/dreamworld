﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using Photon.Realtime;
using Hashtable = ExitGames.Client.Photon.Hashtable;

/// <summary>
/// RandomRoomへの参加管理マネージャー
/// </summary>
namespace DreamWorld
{
    public class RoomManager : MonoBehaviourPunCallbacks
    {
        // Member
        private string m_MapType;

        private void Start()
        {
            PhotonNetwork.AutomaticallySyncScene = true;
        }

        #region UI Callback Methods
        /// <summary>
        /// RandomRoomへ参加するメソッド
        /// </summary>
        public void JoinRandomRoom()
        {
            PhotonNetwork.JoinRandomRoom();
        }

        /// <summary>
        /// ボタン押下時、そのRoomへ参加する
        /// </summary>
        public void OnEnterButtonClicked_Outdoor()
        {
            m_MapType = MultiplayerVRConstants.MAP_TYPE_VALUE_OUTDOOR;
            //予想されるPlayerの最大値
            byte expectedPlayerMaxBNums = 0;
            Hashtable expectedCustomRoomProperties = new Hashtable() { { MultiplayerVRConstants.MAP_TYPE_KEY, m_MapType } };
            PhotonNetwork.JoinRandomRoom(expectedCustomRoomProperties, expectedPlayerMaxBNums);
        }

        /// <summary>
        /// ボタン押下時、そのRoomへ参加する
        /// スクリプト内のデータにアクセスしランダムルーム参加できなかった場合に、OnJoinRandomFailedにまずアクセスする
        /// </summary>
        public void OnEnterButtonClicked_School()
        {
            m_MapType = MultiplayerVRConstants.MAP_TYPE_VALUE_SCHOOL;
            //予想されるPlayerの最大値
            byte expectedPlayerMaxBNums = 0;
            Hashtable expectedCustomRoomProperties = new Hashtable() { { MultiplayerVRConstants.MAP_TYPE_KEY, m_MapType} };
            PhotonNetwork.JoinRandomRoom(expectedCustomRoomProperties, expectedPlayerMaxBNums);
        }   
        #endregion

        #region Photon Callback Methods
        /// <summary>
        /// RandomRoom参加に失敗したときに呼び出されるメソッド
        /// </summary>
        /// <param name="returnCode">ErrorCode</param>
        /// <param name="message">ErrorMessage</param>
        public override void OnJoinRandomFailed(short returnCode, string message)
        {
            Debug.Log($"ルームの作成に失敗しました: {returnCode} : {message}");
            CreateAndJoinRoom();
        }

        /// <summary>
        /// Roomを作成するときに呼び出される
        /// </summary>
        public override void OnCreatedRoom()
        {
            var currenRoomName = PhotonNetwork.CurrentRoom.Name;
            Debug.Log($"{currenRoomName} という部屋の名前でRoomが作成されました");
        }

        /// <summary>
        /// ローカルプレイヤーが一人の時に
        /// 作成した部屋に自動的に参加したときに呼び出される
        /// </summary>
        public override void OnJoinedRoom()
        {
            var playerNickName = PhotonNetwork.NickName;
            var currentRoomNamne = PhotonNetwork.CurrentRoom.Name;
            var currentRoomPlayerCount = PhotonNetwork.CurrentRoom.PlayerCount;
            Debug.Log($"ローカルプレイヤ―のニックネーム : {playerNickName} \n 現在の部屋の名前 ： {currentRoomNamne} \n 現在の部屋のプレイヤー数 ： {currentRoomPlayerCount}");


            if (!CheckContainsKey_MAP_TYPE_KEY()) return;
            
            object mapType;
            if (PhotonNetwork.CurrentRoom.CustomProperties.TryGetValue(MultiplayerVRConstants.MAP_TYPE_KEY, out mapType))
            {
                Debug.Log(mapType.ToString() + " : マップの部屋の参加した");
                if(mapType.ToString() == MultiplayerVRConstants.MAP_TYPE_VALUE_SCHOOL)
                {
                    // Load the School Scene
                    PhotonNetwork.LoadLevel((int)SceneIndexes.SCHOOL);
                }
                else if(mapType.ToString() == MultiplayerVRConstants.MAP_TYPE_VALUE_OUTDOOR)
                {
                    // Load the Outdoor Scene
                    PhotonNetwork.LoadLevel((int)SceneIndexes.OUTDOOR);
                }
            }
        }

        /// <summary>
        /// 他のプレイヤーが部屋に参加したときに呼び出される
        /// </summary>
        /// <param name="newPlayer">新しく参加したプレイヤ―のデータ</param>
        public override void OnPlayerEnteredRoom(Player newPlayer)
        {
            var currentRoomPlayerCount = PhotonNetwork.CurrentRoom.PlayerCount;
            Debug.Log($"{newPlayer.NickName} : が新しく参加し、現在の部屋のプレイヤー数は、{currentRoomPlayerCount}です。");
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// MultiplayerVRConstants.MAP_TYPE_KEYが取得できるかチェック
        /// </summary>
        /// <returns>Keyが取得できなければFalseを返す</returns>
        private bool CheckContainsKey_MAP_TYPE_KEY()
        {
            return PhotonNetwork.CurrentRoom.CustomProperties.ContainsKey(MultiplayerVRConstants.MAP_TYPE_KEY);
        }

        /// <summary>
        /// ルームに参加に失敗した場合、Photonでルームを作成する
        /// </summary>
        private void CreateAndJoinRoom()
        {
            var randomRoomName = $"Room_{Random.Range(0, 10001)}";
            RoomOptions roomOptions = new RoomOptions();
            roomOptions.MaxPlayers = 20;

            string[] roomPropsInLobby = { MultiplayerVRConstants.MAP_TYPE_KEY };
            Hashtable customRoomProperties = new Hashtable() { { MultiplayerVRConstants.MAP_TYPE_KEY, m_MapType } };
            // 部屋を設定
            roomOptions.CustomRoomPropertiesForLobby = roomPropsInLobby;
            roomOptions.CustomRoomProperties = customRoomProperties;

            PhotonNetwork.CreateRoom(randomRoomName, roomOptions);
        }
        #endregion

    }
}
