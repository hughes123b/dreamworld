﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine.UI;

namespace DreamWorld
{
    public class DOTweenManager : MonoBehaviour
    {
        // Inspector
        [SerializeField, Range(1f, 2f), Tooltip("Scaleの変更比率")]
        private float m_Ratio = 1.04f;
        [SerializeField, Range(1f, 2f), Tooltip("Animationの遅延速度")]
        private float m_Duration = 1.25f;
        // Member
        protected float Ratio { get => m_Ratio; }
        protected float Duration { get=> m_Duration; }

        #region Unity Methods
        private void Start()
        {
        }

        private void Update()
        {
        }
        #endregion
    }
}