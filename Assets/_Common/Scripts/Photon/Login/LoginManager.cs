﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

/// <summary>
/// Login管理スクリプト
/// </summary>
namespace DreamWorld
{
    public class LoginManager : MonoBehaviourPunCallbacks
    {
        // Inspector
        [SerializeField, Tooltip("LoginUIプレハブ > ConnectWithName_Panel > Input Fieldを設定")]
        private TMP_InputField m_PlayerName_InputName = null;
        [SerializeField]
        private GameObject m_VR_keys = null;

        #region UI Callback Methods
        /// <summary>
        /// 匿名で接続
        /// </summary>
        public void ConnectAnonymously()
        {
            PhotonNetwork.ConnectUsingSettings();
        }
        
        /// <summary>
        /// InputFieldの名前がnullOr空でないとき、入力した名前で部屋に入室できる
        /// </summary>
        public void ConnectToPhotonServer()
        {
            var playerName = m_PlayerName_InputName.text;
            if (!string.IsNullOrWhiteSpace(playerName))
            {
                m_VR_keys.SetActive(false);
                PhotonNetwork.NickName = m_PlayerName_InputName.text;       // AvaterのニックネームをInputFieldに入力したものにする
                PhotonNetwork.ConnectUsingSettings();                       // サーバーに接続
            }

        }

        #endregion

        #region Photon Callback Methods
        /// <summary>
        /// 呼び出されるとPhotonサーバに接続可能な状態
        /// </summary>
        public override void OnConnected()
        {
            Debug.Log("OnConntected is called.");
        }

        /// <summary>
        /// 呼び出されるとユーザーがMasterServerに正常に接続されている
        /// </summary>
        public override void OnConnectedToMaster()
        {
            Debug.Log("OnConnectedToMaster is Called!Player NickName"　+ PhotonNetwork.NickName);
            PhotonNetwork.LoadLevel(SceneManager.GetActiveScene().buildIndex + 1);
        }
        #endregion
    }
}
