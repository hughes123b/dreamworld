﻿/// <summary>
/// マップをキーとした、マップタイプを指定するスクリプト
/// RoomManager.csで任意のマップタイプに参加するときに参照する
/// </summary>
namespace DreamWorld
{
    public sealed class MultiplayerVRConstants
    {
        // Room Type
        public static readonly string MAP_TYPE_KEY = "map";
        public static readonly string MAP_TYPE_VALUE_SCHOOL = "school";
        public static readonly string MAP_TYPE_VALUE_OUTDOOR = "outdoor";
    }
}
