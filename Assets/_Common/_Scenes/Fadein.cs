using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace DreamWorld
{
    public class Fadein : MonoBehaviour
    {
        public CanvasGroup CanvasGroup;
        public Image image;
        // Start is called before the first frame update
        void Start()
        {
            CanvasGroup = image.GetComponent<CanvasGroup>();
            CanvasGroputFadeInOut.FadeOut(CanvasGroup, 1.0f);
        

        }

        // Update is called once per frame
        void Update()
        {
        
        }
    }
}
