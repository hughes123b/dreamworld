using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

namespace DreamWorld
{
    public sealed class VirtualWorldManager : MonoBehaviourPunCallbacks
    {
        #region Photon Callback Methods
        /// <summary>
        /// 他のプレイヤーが部屋に参加したときに呼び出される
        /// </summary>
        /// <param name="newPlayer">新しく参加したプレイヤ―のデータ</param>
        public override void OnPlayerEnteredRoom(Player newPlayer)
        {
            var currentRoomPlayerCount = PhotonNetwork.CurrentRoom.PlayerCount;
            Debug.Log($"{newPlayer.NickName} : が新しく参加し、現在の部屋のプレイヤー数は、{currentRoomPlayerCount}です。");
        }
        #endregion
    }
}
