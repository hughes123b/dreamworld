### Dream World
## システム概要
- *Dream World* ：Photonを利用し、マルチプレイヤーでPlay時に選択できるRoomに入室してChatや
Player(アバター)自身が変身し、Roomを探索しアイテムを拾うことができるなど、簡単なゲーム要素を取り入れているコミュニケーションツールです。
主に、VRの世界ではコミュニケーションツールとしてニーズが高いことに着目し、開発に至っています。

## 環境
- Oculus Quest2 or Oculus Quest
- Unity 2021.3.0f1 Personal
- Visual Studio 2022
- Visual Studio Code 1.70.1
- Photon (PUN2 - Free) : 2020.3.16

## APKファイルのインストール方法
